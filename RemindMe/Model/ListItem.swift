//
//  ListItem.swift
//  RemindMe
//
//  Created by Tyler McCormick on 1/29/19.
//  Copyright © 2019 Tyler McCormick. All rights reserved.
//

import Foundation

class ListItem : Codable {
    var title : String = ""
    var done : Bool = false
}
