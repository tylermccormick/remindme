//
//  Category.swift
//  RemindMe
//
//  Created by Tyler McCormick on 2/3/19.
//  Copyright © 2019 Tyler McCormick. All rights reserved.
//

import Foundation
import RealmSwift
import ChameleonFramework

class Category : Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var color: String = ""

    
    let items = List<Item>()
    
}
