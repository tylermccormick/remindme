//
//  Item.swift
//  RemindMe
//
//  Created by Tyler McCormick on 2/3/19.
//  Copyright © 2019 Tyler McCormick. All rights reserved.
//

import Foundation
import RealmSwift

class Item : Object {
    
    @objc dynamic var name: String = ""
    @objc dynamic var done: Bool = false
    @objc dynamic var date: Date?
    
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
    
}
