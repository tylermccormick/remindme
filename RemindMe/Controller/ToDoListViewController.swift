//
//  ViewController.swift
//  RemindMe
//
//  Created by Tyler McCormick on 1/28/19.
//  Copyright © 2019 Tyler McCormick. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class ToDoListViewController: SwipeTableViewController {

    let realm = try! Realm()
    var items: Results<Item>?
    var categoryColor : String = ""
    var defaultColor : String = "7B24A2"
    
    var selectedCategory : Category? { // creates an optional category that we can use to pull items from and loads items once it is set.
        didSet {
            LoadFromDataFile()
        }
    }



    @IBOutlet weak var reminderSearch: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = selectedCategory?.name
        self.reminderSearch.backgroundImage = UIImage()
        tableView.separatorStyle = .none
        categoryColor = (selectedCategory?.color)!
    }
   override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        setNavBarStyle(color: categoryColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        setNavBarStyle(color: defaultColor)
        
    }
    
    //MARK: Set Navbar Style
    func setNavBarStyle(color: String) {
        guard let navBar = navigationController?.navigationBar else {fatalError("Navbar does not exist.")}
        navBar.barTintColor = UIColor(hexString: color)
        navBar.tintColor = UIColor(contrastingBlackOrWhiteColorOn: UIColor(hexString: color)!, isFlat: true )
        navBar.isTranslucent = true
        reminderSearch.backgroundColor = UIColor(hexString: color)
        
        
    }
    
    //MARK: Initialize TableView
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        
        if let item = items?[indexPath.row] {
        cell.textLabel?.text = items?[indexPath.row].name// Sets the text label of the cell to the value of in the array based on the row number of the table view.
        if let color = UIColor(hexString: selectedCategory?.color ?? "7B24A2")?.darken(byPercentage: CGFloat(indexPath.row + 1) / CGFloat(items!.count + 2))
        {
            cell.backgroundColor = color
        }
        cell.textLabel?.textColor = UIColor(contrastingBlackOrWhiteColorOn: cell.backgroundColor!, isFlat: true)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
            
            
        cell.accessoryType = item.done == true ? .checkmark : .none // uses the turnery operator on the accessoryType to decide if the done attribute is true, apply a checkmark. Else apply none.
        } else {
            cell.textLabel?.text = "Add A New Item"
            cell.accessoryType = .none
        }
        
        return cell
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 1 // sets the total number of rows to the number of items in the array.
    }
    
    //MARK: TableView Interactions
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let item = items?[indexPath.row] {
            do {
                try realm.write {
                    item.done = !item.done
                }
            }
            catch {
                print("Could not update data \(error)")
            }
        }
        tableView.deselectRow(at: indexPath, animated: true) // this will unhighlight the row after we touch it an animate it.
        tableView.reloadData() // reloads the data to reflect the change in the done attribute
    }
    
    
    //MARK: Add Button Functionality
    
    @IBAction func addReminderPressed(_ sender: UIBarButtonItem) {
       
        var reminderTextInput = UITextField() // Creates a text field that is accessable within this scope.
        
        let alert = UIAlertController.init(title: "Add New Reminder", message: "", preferredStyle: .alert) // creates the alert container.
        
        let action = UIAlertAction(title: "Add Reminder", style: .default) { (action) in
            // creates a button to carry out the action.

            if let currentCategory =  self.selectedCategory {
                do {
                    try self.realm.write {
                        let newItem = Item() // defines the new item
                        newItem.name = reminderTextInput.text! // sets the name of the item
                        newItem.date = Date()
                        currentCategory.items.append(newItem)
                    }
                }
                catch {
                    print("Could not write data: \(error)")
                }
                
            }
            self.tableView.reloadData() // relods the tableview to reflect our new data entry.
        }
        
        alert.addTextField { (reminderText) in // adds a text box to the alert box.
            reminderText.placeholder = "Create New Reminder"
            reminderTextInput = reminderText // sets the textfield in the alert to the one we defined so we can access it using the action.
        }
        
        alert.addAction(action) // adds the action to the alert box.
        
        present(alert, animated: true, completion: nil) // finally, shows the alert with all components we defined above.
    }
    
    //MARK: Delete Data
    override func updateModel(at indexPath: IndexPath) {
        if let itemForDeletion = self.items?[indexPath.row] {
            do {
                try self.realm.write {
                    self.realm.delete(itemForDeletion)
                }
            }
            catch {
                print("Could not update data \(error)")
            }
        }
    }
    
    //MARK: Save/Load Data
    func save(item: Item) {
        
        do {
            try realm.write {
                realm.add(item)
            }
        }
        catch {
            print("Data could not be saved, \(error)")
        }
    }

    func LoadFromDataFile() {
 
        items = selectedCategory?.items.sorted(byKeyPath: "name", ascending: true)
        tableView.reloadData() // reloads the data to reflect the change in the search
    }
    
}

//MARK: Search Bar
extension ToDoListViewController: UISearchBarDelegate {

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        items = items?.filter("name CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "date", ascending: true)
        tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0 {
            LoadFromDataFile()
            DispatchQueue.main.async { // runs the code asynchonously in the foreground thread
                searchBar.resignFirstResponder() // the search bar stops being the active item.
            }

        }
    }

}

