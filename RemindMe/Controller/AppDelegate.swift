//
//  AppDelegate.swift
//  RemindMe
//
//  Created by Tyler McCormick on 1/28/19.
//  Copyright © 2019 Tyler McCormick. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        print(Realm.Configuration.defaultConfiguration.fileURL)
        
        do {
        let realm = try Realm()
        }
        catch {
            print("Could not load realm: \(error)")
        }

        return true
    }


}

