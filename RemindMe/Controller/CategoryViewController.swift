//
//  CategoryViewController.swift
//  RemindMe
//
//  Created by Tyler McCormick on 1/30/19.
//  Copyright © 2019 Tyler McCormick. All rights reserved.
//

import UIKit
import RealmSwift
import ChameleonFramework

class CategoryViewController: SwipeTableViewController {

    let realm = try! Realm()
    var categories: Results<Category>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        load()
    }
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(false)
    }

  
    // sets the text label in the row to the category name.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.textLabel?.text = categories?[indexPath.row].name ?? "Add A Category"
        cell.backgroundColor = UIColor(hexString: (categories?[indexPath.row].color) ?? "7B24A2")
        cell.textLabel?.textColor = UIColor(contrastingBlackOrWhiteColorOn: cell.backgroundColor!, isFlat: true)
        tableView.separatorStyle = .none
        
        return cell
    }
    
    // sets the  initial number of rows to the length of the array.
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories?.count ?? 1
    }
    
    // interacting with the list.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! ToDoListViewController // sets the destination view controller to our to-do list.
        if let indexPath = tableView.indexPathForSelectedRow {
            destinationVC.selectedCategory = categories?[indexPath.row]
        }
        
    }
    
    // actions are performed if the add button is pressed.
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var categoryTextInput = UITextField()
        
        let alert = UIAlertController.init(title: "Add New Category", message: "", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Add Category", style: .default) { (action) in
            
            let newCategory = Category()
            newCategory.name = categoryTextInput.text!
            newCategory.color = UIColor.randomFlat.hexValue()
            self.save(category: newCategory)
            
            
            self.tableView.reloadData()
        }
        
        alert.addTextField { (categoryText) in
            categoryText.placeholder = "Create New Category"
            categoryTextInput = categoryText
        }
        
        alert.addAction(action) //
        
        present(alert, animated: true, completion: nil)
    }
    
    //MARK: Delete Data
    override func updateModel(at indexPath: IndexPath) {
        if let categoryForDeletion = self.categories?[indexPath.row] {
            do {
                try self.realm.write {
                    self.realm.delete(categoryForDeletion)
                }
            }
            catch {
                print("Could not update data \(error)")
            }
        }
    }
    
    //MARK: Save/Load Data
    func save(category: Category) {
        
        do {
            try realm.write {
                realm.add(category)
            }
        }
        catch {
            print("Data could not be saved, \(error)")
        }
    }
    
    func load() {
        categories = realm.objects(Category.self)
        
        tableView.reloadData()
    }
    
}

